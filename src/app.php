<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add(
    'test',
    new Routing\Route('/test/{name}', [
        'name'        => null,
        '_controller' => 'Controller\TestController::index',
    ])
);
$routes->add(
    'month',
    new Routing\Route('/month/{month}/{year}', [
        'month'       => null,
        'year'        => null,
        '_controller' => 'Controller\MonthController::index',
    ])
);

return $routes;
