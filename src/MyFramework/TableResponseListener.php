<?php

namespace MyFramework;

use MyFramework\Response\TableResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Twig\Environment;

class TableResponseListener implements EventSubscriberInterface
{
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function onView(ViewEvent $event)
    {
        $response = $event->getControllerResult();
        if ($response instanceof TableResponse) {
            $renderedContent = $this->twig->render(
                'month.html.twig',
                [
                    'headers'       => $response->getHeaders(),
                    'table_content' => $response->getData(),
                ]
            );

            $event->setResponse(new Response($renderedContent));
        }
    }

    public static function getSubscribedEvents()
    {
        return ['kernel.view' => 'onView'];
    }
}
