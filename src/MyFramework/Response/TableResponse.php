<?php

namespace MyFramework\Response;

class TableResponse
{
    /**
     * @var array
     */
    private $headers;
    /**
     * @var array
     */
    private $data;

    public function __construct(array $headers, array $data)
    {
        $this->headers = $headers;
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
