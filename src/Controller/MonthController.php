<?php

namespace Controller;

use MyFramework\Response\TableResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MonthController
{
    /**
     * @param Request $request
     * @param int     $month
     * @param int     $year
     * @return TableResponse|Response
     */
    public function index(Request $request, int $month, int $year)
    {
        $errors = [];
        if (!is_int($month) || $month < 1 || $month > 12) {
            $errors[] = 'Меcяц должен быть целым числом от 1 до 12';
        }
        if (!is_int($year) || $year < 1) {
            $errors[] = 'Год должен быть целым числом от больше 0';
        }
        if (!empty($errors)) {
            return new Response(implode('</br>', $errors));
        }
        $data = [];

        $day = 1;
        $endOfMonth = false;
        while (!$endOfMonth) {
            $time = mktime(12, 0, 0, $month, $day, $year);
            $data[] = [
                'week_day_name' => date('l', $time),
                'day_number'    => $day,
                'month_name'    => date('F', $time),
                'yeat'          => $year,
            ];
            $day++;
            $endOfMonth = ((int)date('m', $time) !== $month);
        }

        $headers = [
            'week_day_name' => 'День недели',
            'day_number'    => 'Число',
            'month_name'    => 'Месяц',
            'yeat'          => 'Год',
        ];

        return new TableResponse($headers, $data);
    }
}
