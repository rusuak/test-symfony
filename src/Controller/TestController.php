<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController
{
    /**
     * @param Request     $request
     * @param string|null $name
     * @return Response
     */
    public function index(Request $request, string $name = null): Response
    {
        return new Response($this->concat('Hello', $name));
    }

    /**
     * @param string $str1
     * @param string $str2
     * @return string
     */
    private function concat(string $str1, string $str2): string
    {
        return $str1 . ' ' . $str2;
    }
}
